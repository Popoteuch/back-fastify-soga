const fastify = require('fastify')({ logger: true, pluginTimeout: 62000 })

fastify.register(require('fastify-cors'), { origin: '*' })
fastify.register(require('./plugin/mongo'))
fastify.register(require('./plugin/middleware/auth_middleware'))
fastify.register(require('./plugin/routes/auth_router'))
fastify.register(require('./plugin/routes/auth'), { prefix: '/auth' })
fastify.register(require('./plugin/routes/bill'), { prefix: '/bill' })
fastify.register(require('./plugin/routes/society'), { prefix: '/society' })
fastify.register(require('./plugin/routes/user'), { prefix: '/user' })
fastify.register(require('./plugin/routes/user_society'), { prefix: '/user_society' })
fastify.register(require('fastify-jwt'), {
  secret: process.env.JWT_SECRET
})
fastify.get('/', function (request, reply) {
  reply.send({ hello: 'world' })
})

const start = async () => {
  try {
    await fastify.listen(process.env.PORT || 5000, '127.0.0.1')
  } catch (err) {
    fastify.log.error(err)
  }
}

start()
