const {
  login: loginSchema,
  registration: registrationSchema
} = require('./schemas')

const errors = require('../errors')
const boom = require('boom')

async function routes (fastify, options, done) {
  const userCollection = fastify.mongo.db.collection('user')

  fastify.setErrorHandler(function (error, request, reply) {
    const message = error.message
    if (errors[message]) {
      reply.code(412)
    }
    reply.send(error)
  })

  fastify.post('/login', { schema: loginSchema }, async (request, reply) => {
    try {
      const { email, password } = request.body
      if (!email || !password) {
        reply.status(400).send({ msg: errors.MISSING_PARAMETER })
        return
      }
      const userCollection = fastify.mongo.db.collection('user')
      const userData = await userCollection.findOne({ email, password })
      if (userData) {
        const token = fastify.jwt.sign({ id: userData._id }, { expiresIn: 86400 })
        const _id = fastify.mongo.ObjectId(userData._id)
        userCollection.updateOne({ _id }, { $set: { token: token } }, { returnOriginal: false })
        reply.status(200).send({ token, email })
      } else {
        reply.status(400).send({ msg: errors.WRONG_CREDENTIAL })
      }
    } catch (error) {
      throw boom.boomify(error)
    }
  })
  fastify.post('/register', { schema: registrationSchema }, async (request, reply) => {
    const user = request.body
    user.created_at = new Date()
    user.updated_at = new Date()
    user.rank = user.rank || 0
    const result = await userCollection.insertOne(user)
    return { success: result.ops.length === 1 }
  })
  done()
}

module.exports = routes
