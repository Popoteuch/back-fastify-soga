const userProfileOutput = {
  type: 'object',
  require: ['_id', 'email'],
  properties: {
    _id: { type: 'string' },
    email: { type: 'string' },
    rank: { type: 'string' },
    address: { type: 'string' },
    token: { type: 'string' },
    name: { type: 'string' },
    status: { type: 'string' },
    created_at: { type: 'string' },
    updated_at: { type: 'string' }
  }
}

const registration = {
  body: {
    type: 'object',
    required: ['email', 'password', 'name'],
    properties: {
      email: {
        type: 'string'
      },
      password: {
        type: 'string'
      },
      name: {
        type: 'string'
      }
    },
    additionalProperties: false
  },
  response: {
    200: {
      type: 'object',
      required: ['success'],
      properties: {
        success: { type: 'boolean' }
      },
      additionalProperties: false
    }
  }
}

const login = {
  body: {
    type: 'object',
    require: ['email', 'password'],
    properties: {
      email: { type: 'string' },
      password: { type: 'string' }
    },
    additionalProperties: false
  },
  response: {
    200: userProfileOutput
  }
}

module.exports = {
  login,
  registration
}
