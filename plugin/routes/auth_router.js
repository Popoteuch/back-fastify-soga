const boom = require('boom')
const errors = require('./errors')
async function routes (fastify, options, done) {
  fastify.post('/api/v1/generateAccessToken', async (req, res) => {
    try {
      const { email, password } = req.body
      if (!email || !password) {
        res.status(400).send({ msg: errors.MISSING_PARAMETER })
        return
      }
      // const userCollection = fastify.mongo.db.collection('user')
      // let userData = await userCollection.findOne({ login, password })
      // if(...) {
      //   generate jwt
      // }
      const token = fastify.jwt.sign({ email, password }, { expiresIn: 86400 })
      res.status(200).send({ token, email })
    } catch (error) {
      throw boom.boomify(error)
    }
  })
  done()
}

module.exports = routes
