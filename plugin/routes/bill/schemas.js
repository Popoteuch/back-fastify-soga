const billProfileOutput = {
  type: 'object',
  require: ['_id', 'idSociety'],
  properties: {
    _id: { type: 'string' },
    idSociety: { type: 'string' },
    amount: { type: 'integer' },
    name: { type: 'string' },
    date: { type: 'string' },
    number: { type: 'string' }
  },
  additionalProperties: false
}
const getOpt = {
  schema: {
    response: {
      200: billProfileOutput
    }
  }
}
const getOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: billProfileOutput
      }
    }
  }
}
const postOpt = {
  schema: {
    body: {
      type: 'object',
      properties: {
        idSociety: { type: 'string' },
        amount: { type: 'integer' },
        name: { type: 'string' },
        date: { type: 'string' },
        number: { type: 'string' },
        status: { type: 'string' }
      }
    },
    response: {
      200: billProfileOutput
    }
  }
}
const putOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}
const deleteOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}

module.exports = {
  getOpt,
  getOpts,
  postOpt,
  putOpt,
  deleteOpt
}
