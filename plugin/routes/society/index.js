const {
  getOpt: getOptSchema,
  getOpts: getOptsSchema,
  postOpt: postOptSchema,
  putOpt: putOptSchema,
  deleteOpt: deleteOptSchema
} = require('./schemas')

const errors = require('../errors')

function routes (fastify, options, done) {
  const societyCollection = fastify.mongo.db.collection('society')

  fastify.get('/:id', { schema: getOptSchema }, async (request, reply) => {
    const { id } = request.params
    const _id = fastify.mongo.ObjectId(id)
    const society = await societyCollection.findOne({ _id })
    return society
  })
  fastify.post('/', { schema: postOptSchema }, async (request, reply) => {
    const society = request.body
    society.created_at = new Date()
    society.updated_at = new Date()
    society.status = society.status || 1
    const result = await societyCollection.insertOne(society)
    return { success: result.ops.length === 1 }
  })
  fastify.put('/:id', { schema: putOptSchema }, async (request, reply) => {
    const { id } = request.params
    const _id = fastify.mongo.ObjectId(id)
    const body = request.body
    body.updated_at = new Date()
    const result = await societyCollection.findOneAndUpdate({ _id }, { $set: body }, { returnOriginal: false })
    return { success: result.ok === 1 }
  })

  fastify.register(function (fastify, options, done) {
    fastify.addHook('preHandler', async (request, reply) => {
      const userCollection = fastify.mongo.db.collection('user')
      const token = await request.jwtVerify()
      const user = await userCollection.findOne({ _id: fastify.mongo.ObjectId(token.id) })
      if (!(user.rank ?? 0)) {
        const err = new Error()
        err.statusCode = 401
        err.message = errors.NOT_AUTHORIZED
        throw err
      }
    })

    fastify.get('/', { schema: getOptsSchema }, async (request, reply) => {
      const societies = await societyCollection.find().toArray()
      return societies
    })
    fastify.delete('/:id', { schema: deleteOptSchema }, async (request, reply) => {
      const { id } = request.params
      const _id = fastify.mongo.ObjectId(id)
      const deletedSociety = await societyCollection.deleteOne({ _id })
      return { success: deletedSociety.deletedCount === 1 }
    })
    done()
  })
  done()
}

module.exports = routes
