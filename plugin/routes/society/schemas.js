const societyProfileOutput = {
  type: 'object',
  require: ['_id', 'name'],
  properties: {
    _id: { type: 'string' },
    name: { type: 'string' },
    siret: { type: 'string' },
    address: { type: 'string' },
    mail: { type: 'string' },
    phone: { type: 'string' },
    logo: { type: 'string' }
  },
  additionalProperties: false
}
const getOpt = {
  schema: {
    response: {
      200: societyProfileOutput
    }
  }
}
const getOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: societyProfileOutput
      }
    }
  }
}
const postOpt = {
  schema: {
    body: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        siret: { type: 'string' },
        address: { type: 'string' },
        mail: { type: 'string' },
        phone: { type: 'string' },
        logo: { type: 'string' },
        status: { type: 'string' }
      }
    },
    response: {
      200: societyProfileOutput
    }
  }
}
const putOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}
const deleteOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  }
}

module.exports = {
  getOpt,
  getOpts,
  postOpt,
  putOpt,
  deleteOpt
}
