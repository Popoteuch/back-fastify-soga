const {
  getOpt: getOptSchema,
  getOpts: getOptsSchema,
  putOpt: putOptSchema,
  deleteOpt: deleteOptSchema
} = require('./schemas')

const errors = require('../errors')

async function routes (fastify, options, done) {
  const userCollection = fastify.mongo.db.collection('user')

  fastify.decorate('auth', (request) => request.headers.auth === 'secret')

  fastify.setErrorHandler(function (error, request, reply) {
    const message = error.message
    if (errors[message]) {
      reply.code(412)
    }
    reply.send(error)
  })

  fastify.get('/:id',
    {
      schema: getOptSchema,
      preValidation: [fastify.jwtauthentication]
    }, async (request, reply) => {
      const { id } = request.params
      const _id = fastify.mongo.ObjectId(id)
      const user = await userCollection.findOne({ _id })
      return user
    })
  fastify.put('/:id',
    {
      schema: putOptSchema,
      preValidation: [fastify.jwtauthentication]
    }, async (request, reply) => {
      const { id } = request.params
      const _id = fastify.mongo.ObjectId(id)
      const body = request.body
      body.updated_at = new Date()
      const result = await userCollection.findOneAndUpdate({ _id }, { $set: body }, { returnOriginal: false })
      return { success: result.ok === 1 }
    })

  // Nécessite admin
  fastify.register(function (fastify, options, done) {
    fastify.addHook('preHandler', async (request, reply) => {
      const userCollection = fastify.mongo.db.collection('user')
      const token = await request.jwtVerify()
      const user = await userCollection.findOne({ _id: fastify.mongo.ObjectId(token.id) })
      if (!(user.rank ?? 0)) {
        const err = new Error()
        err.statusCode = 401
        err.message = errors.NOT_AUTHORIZED
        throw err
      }
    })
    fastify.get('/',
      {
        schema: getOptsSchema,
        preValidation: [fastify.jwtauthentication]
      }, async (request, reply) => {
        const users = await userCollection.find().toArray()
        return users
      })
    fastify.delete('/:id',
      {
        schema: deleteOptSchema,
        preValidation: [fastify.jwtauthentication]
      }, async (request, reply) => {
        const { id } = request.params
        const _id = fastify.mongo.ObjectId(id)
        const deletedUser = await userCollection.deleteOne({ _id })
        return { success: deletedUser.deletedCount === 1 }
      })
    done()
  })
  done()
}

module.exports = routes
