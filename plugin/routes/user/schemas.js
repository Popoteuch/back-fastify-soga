const userProfileOutput = {
  type: 'object',
  require: ['_id', 'email'],
  properties: {
    _id: { type: 'string' },
    email: { type: 'string' },
    rank: { type: 'integer' },
    address: { type: 'string' },
    name: { type: 'string' }
  },
  additionalProperties: false
}
const getOpt = {
  schema: {
    response: {
      200: userProfileOutput
    }
  }
}
const getOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: userProfileOutput
      }
    }
  }
}
const putOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}
const deleteOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  }
}

const registration = {
  body: {
    type: 'object',
    required: ['email', 'password'],
    properties: {
      email: {
        type: 'string'
      },
      password: {
        type: 'string'
      }
    },
    additionalProperties: false
  },
  response: {
    200: {
      type: 'object',
      required: ['_id'],
      properties: {
        userId: { type: 'string' }
      },
      additionalProperties: false
    }
  }
}

const login = {
  body: {
    type: 'object',
    require: ['email', 'password'],
    properties: {
      email: { type: 'string' },
      password: { type: 'string' }
    },
    additionalProperties: false
  },
  response: {
    200: userProfileOutput
  }
}

module.exports = {
  getOpt,
  getOpts,
  putOpt,
  deleteOpt,
  login,
  registration
}
