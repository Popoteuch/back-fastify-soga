const {
  getOpt: getOptSchema,
  getOpts: getOptsSchema,
  postOpt: postOptSchema,
  putOpt: putOptSchema,
  deleteOpt: deleteOptSchema
} = require('./schemas')

const errors = require('../errors')

function routes (fastify, options, done) {
  const userSocietyCollection = fastify.mongo.db.collection('user_society')

  fastify.get('/user/:id', { schema: getOptSchema }, async (request, reply) => {
    const id = fastify.mongo.ObjectId(request.params.id)
    const userSociety = await userSocietyCollection.findOne({ idUser: id })
    return userSociety
  })
  fastify.get('/society/:id', { schema: getOptSchema }, async (request, reply) => {
    const id = fastify.mongo.ObjectId(request.params.id)
    const userSociety = await userSocietyCollection.findOne({ idSociety: id })
    return userSociety
  })
  fastify.post('/', { schema: postOptSchema }, async (request, reply) => {
    const userSociety = request.body
    userSociety.idUser = fastify.mongo.ObjectId(userSociety.idUser)
    userSociety.idSociety = fastify.mongo.ObjectId(userSociety.idSociety)
    userSociety.created_at = new Date()
    userSociety.updated_at = new Date()
    userSociety.status = userSociety.status || 1
    const result = await userSocietyCollection.insertOne(userSociety)
    return { success: result.ops.length === 1 }
  })
  fastify.put('/:id', { schema: putOptSchema }, async (request, reply) => {
    const { id } = request.params
    const _id = fastify.mongo.ObjectId(id)
    const body = request.body
    body.updated_at = new Date()
    const result = await userSocietyCollection.findOneAndUpdate({ _id }, { $set: body }, { returnOriginal: false })
    return { success: result.ok === 1 }
  })

  fastify.register(function (fastify, options, done) {
    fastify.addHook('preHandler', async (request, reply) => {
      const userCollection = fastify.mongo.db.collection('user')
      const token = await request.jwtVerify()
      const user = await userCollection.findOne({ _id: fastify.mongo.ObjectId(token.id) })
      if (!(user.rank ?? 0)) {
        const err = new Error()
        err.statusCode = 401
        err.message = errors.NOT_AUTHORIZED
        throw err
      }
    })
    fastify.delete('/:id', { schema: deleteOptSchema }, async (request, reply) => {
      const { id } = request.params
      const _id = fastify.mongo.ObjectId(id)
      const deletedUserSociety = await userSocietyCollection.deleteOne({ _id })
      return { success: deletedUserSociety.deletedCount === 1 }
    })
    fastify.get('/', { schema: getOptsSchema }, async (request, reply) => {
      const usersSocieties = await userSocietyCollection.find().toArray()
      return usersSocieties
    })
    done()
  })
  done()
}

module.exports = routes
