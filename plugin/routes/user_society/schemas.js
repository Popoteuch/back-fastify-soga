const userSocietyProfileOutput = {
  type: 'object',
  require: ['_id', 'idUser', 'idSociety'],
  properties: {
    _id: { type: 'string' },
    idUser: { type: 'string' },
    idSociety: { type: 'string' }
  },
  additionalProperties: false
}
const getOpt = {
  schema: {
    response: {
      200: userSocietyProfileOutput
    }
  }
}
const getOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: userSocietyProfileOutput
      }
    }
  }
}
const postOpt = {
  schema: {
    body: {
      type: 'object',
      properties: {
        idUser: { type: 'string', ref: 'user' },
        idSociety: { type: 'string', ref: 'society' },
        status: { type: 'string' }
      }
    },
    response: {
      200: userSocietyProfileOutput
    }
  }
}
const putOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}
const deleteOpt = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}

module.exports = {
  getOpt,
  getOpts,
  postOpt,
  putOpt,
  deleteOpt
}
