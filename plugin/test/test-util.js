'use strict'

const PASSWORD = 'the-password'

async function login (t, fastify, username) {
  const res = await fastify.inject({
    method: 'POST',
    url: '/api/user/login',
    headers: {
      'Content-type': 'application/json'
    },
    payload: JSON.stringify({
      username: username,
      password: PASSWORD
    })
  })
  t.equal(200, res.statusCode, res.payload)

  return JSON.parse(res.payload).jwt
}

module.exports = {
  login
}
